package com.muamalat.ppib.repository;

import java.util.List;

import com.muamalat.ppib.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    List<Employee> findAll();
    
}