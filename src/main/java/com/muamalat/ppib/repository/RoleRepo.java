package com.muamalat.ppib.repository;

import java.util.List;

import com.muamalat.ppib.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
    
    List<Role> findAll();
}