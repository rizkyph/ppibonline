package com.muamalat.ppib.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.muamalat.ppib.exception.ResourceNotFoundException;
import com.muamalat.ppib.model.Employee;
import com.muamalat.ppib.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

    @Autowired
    private EmployeeRepo employeeRepo;

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        
        return employeeRepo.findAll();
    }

    @GetMapping("/employees/{Id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "Id") Long employeeId) throws ResourceNotFoundException {
        
        Employee employee = employeeRepo.findById(employeeId).orElseThrow(
            () -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId)
        );

        return ResponseEntity.ok().body(employee);
    }
    
    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody final Employee employee) {
        return employeeRepo.save(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") final Long employeeId, @Valid @RequestBody final Employee employeeDetails) throws ResourceNotFoundException {
            
        Employee employee = employeeRepo.findById(employeeId).orElseThrow(
            () -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId)
        );

        employee.setEmailId(employeeDetails.getEmailId());
        employee.setLastName(employeeDetails.getLastName());
        employee.setFirstName(employeeDetails.getFirstName());
        Employee updatedEmployee = employeeRepo.save(employee);
        
        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") final Long employeeId) throws ResourceNotFoundException {
        Employee employee = employeeRepo.findById(employeeId)
       .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));

        employeeRepo.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        
        return response;
    }
}