package com.muamalat.ppib.controller;

import java.util.List;

import com.muamalat.ppib.model.Role;
import com.muamalat.ppib.repository.RoleRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v2")
public class RoleController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/roles")
    public List<Role> getAllRoles() {
        
        return roleRepo.findAll();
    }
}