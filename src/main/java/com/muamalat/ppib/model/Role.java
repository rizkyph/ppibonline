package com.muamalat.ppib.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "roles")
public class Role {

    private Integer roleId;
    private String roleCode;
    private String roleName;
    private String description;
    private Date addOn;
    private Date modiOn;

    public Role() {}

    public Role(String roleCode, String roleName, String description, Date addOn, Date modiOn) {
        this.roleCode = roleCode;
        this.roleName = roleName;
        this.description = description;
        this.addOn = addOn;
        this.modiOn = modiOn;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getRoleId() {
        return roleId;
    }
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Column(name = "role_code", nullable = false)
    public String getRoleCode() {
        return roleCode;
    }
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    @Column(name = "role_name", nullable = false)
    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Column(name = "description", nullable = true)
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "add_on", nullable = true)
    public Date getAddOn() {
        return addOn;
    }
    public void setAddOn(Date addOn) {
        this.addOn = addOn;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "modi_on", nullable = true)
    public Date getModiOn() {
        return modiOn;
    }
    public void setModiOn(Date modiOn) {
        this.modiOn = modiOn;
    }    
}